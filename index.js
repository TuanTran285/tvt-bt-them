// 1: In một bảng số từ 1-100 thỏa mãn điều kiện: (gợi ý dùng 2 vòng for lồng
//     nhau, để ý chỗ bước nhảy)
    
document.write(`<h4 class="text-danger">In Số 1 - 100</h4>`)
document.write("<table class='table'>")
for(let i = 1; i<=10; i++) {
    document.write("<tr>")
    for(let j = 1; j<=10; j++) {
        document.write(`<td>${(i-1) * 10 + j}</td>`)
    }
    document.write("</tr>")
}
document.write("</table>")


// 2: Viết function nhận vào tham số là một mảng số nguyên,tìm và in ra các số
// nguyên tố trong mảng

document.write("<h4>In ra các số nguyên tố trong mảng</h4>")
function timSNTArr(n) {
    let isFlag = true
    if(n < 2) {
        isFlag = false
    }else if(n == 2) {
        isFlag == true
    }else if(n % 2 == 0) {
        isFlag = false
    }else {
        for(let i = 3; i<=Math.sqrt(n); i+=2) {
            if(i % n == 0) {
                isFlag = false
                return;
            }
        }
    }
    return isFlag
    
}

function inDanhSachSNT(arr) {
    for(let i = 0; i<arr.length; i++) {
        const soNT = timSNTArr(arr[i])
        if(soNT) {
            document.write(arr[i] + " ")
        }
    }
}
inDanhSachSNT([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20])

// 3.Viết function nhận vào tham số n, tính S=(2+3+4...+n)+2n

document.write("<h4>Tính Tổng tham số n</h4>")
function tinhTong(n) {
    let result = 0
    for(let i = 1; i<=n; i++) {
        result += i
    }
    document.write(result)
} 
tinhTong(20)


// 4.Viết function nhận vào tham số n, tính số lượng ước số của n. Vd: n = 8 =>
// Ước số của 8 là : 8,4,2,1

document.write("<h4>Tính số lượng ước số của n</h4>")
function uocSo(n) {
    let count = 0
    for(let i = 1; i<=n; i++) {
        if(n % i == 0) {
            count++
        }
    }
    document.write(count)
}
uocSo(8)


// 5.Viết chương trình tìm số đảo ngược của 1 số nguyên dương n nhập từ bàn
// phím.Vd :1234 => 4321 (Gợi y: duyệt chuỗi giống như duyệt mảng)

document.write("<h4>Viết chương trình tìm số đảo ngược của 1 số nguyên dương n</h4>")

function daoNguocSo(n) {
    const result = n.toString().split('').reverse().join('')
    document.write(result)
}
daoNguocSo(1234)

// 6.Tìm x nguyên dương lớn nhất, biết 1+2+3+...+x ≤100

document.write("<h4>Viết chương trình tìm x nguyên dương lớn nhất</h4>")

function timNguyenDuongLN(x) {
    let sum  = 0
    let max = 0
    for(let i = 1; i<=x; i++) {
        sum += i
        if(sum <= x) {
            max = i
        }else {
            break
        }
    }
    document.write(max)
}
timNguyenDuongLN(100)

// 7. Viết function nhận vào số n, in ra bảng cửu chương tương ứng với số đó.
// Vd: n = 3
document.write("<h4>In bảng cửu trương</h4>")
function inBangCuuChuong(n) {
    for(let i = 0; i<=10; i++) {
        document.write(`<div>${n + " * " + i + " = " + n * i}</div>`)
    }

}
inBangCuuChuong(9)

// 8.
document.write("<h4>Yêu cầu viết code để chia cho mỗi player 3 lá theo luật, chia lần lượt mỗi người 1 lá</h4>")

function chiaBai(players, cards) {
    for (let i = 0; i < cards.length; i++) {
        players[i % 4].push(cards[i]);
    }
}

let players = [[], [], [], []]
let cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"]
chiaBai(players, cards)
document.write(`[${players[0]}], [${players[1]}], [${players[2]}], [${players[3]}]`)

document.write("<h4>Tìm số chó và số gà</h4>")

function findDogAndCkicken(m, n) {
    // tính số gà
    let dog = (n -2 * m) / 2
    // tính số gà
    let chicken = m - dog
    return {
        dog,
        chicken
    }

}
const result =  findDogAndCkicken(36, 100)
document.write("Số chó: ", result.dog)
document.write("<br>")
document.write("Số gà: ", result.chicken)


document.write("<h4>Tìm góc lệch giữa kim giờ và kim phút</h4>")

// Hàm tính góc lệch giữa kim giờ và kim phút
function calcAngle(hours, minutes) {
    let hourAngle = (360/12)*(hours % 12) + (30/60)*minutes;
    let minuteAngle = 6*minutes;
    return Math.abs(hourAngle - minuteAngle);
}

// Ví dụ với 6 giờ 10 phút
let hours = 6;
let minutes = 10;
let angle = calcAngle(hours, minutes);
document.write("Góc lệch giữa kim giờ và kim phút là: " + angle + " độ");
